package storage

import (
	"database/sql"
	// pgx driver to keep using standard go sql interfaces
	_ "github.com/jackc/pgx/v4/stdlib"
	"log"
	"mlf/stuff.control/pkg/thinger"
	"strings"
)

type postgresStorage struct {
	db *sql.DB
	l  *log.Logger
}

// NewPostgres creates a new database postgres
func NewPostgres(dbURL string, l *log.Logger) (*postgresStorage, error) {
	db, err := sql.Open("pgx", dbURL)
	if err != nil {
		return nil, err
	}
	err = db.Ping()
	if err != nil {
		return nil, err
	}
	return &postgresStorage{db, l}, nil
}

func (s *postgresStorage) AddThing(t thinger.Thing) error {
	tx, err := s.db.Begin()
	if err != nil {
		return err
	}
	defer tx.Rollback()

	// create thing
	createThing := `
		insert into things(id, name, description, active, img, bought)
		 values ($1, $2, $3, $4, $5, $6)`
	_, err = tx.Exec(createThing, t.ID, t.Name, t.ShortDesc, t.Active, t.Img, t.Bought)
	if err != nil {
		return err
	}

	addLabel := `insert into thing_label(thing_id, label_name) values ($1, $2)`
	for _, label := range t.Labels {
		_, err = tx.Exec(addLabel, t.ID, label)
		if err != nil {
			return err
		}
	}

	return tx.Commit()
}

func (s *postgresStorage) GetThing(id string) (thinger.Thing, error) {
	query := `
			select t.*,
       				coalesce(string_agg(tl.label_name, ','), '') as labels
			from things t
				left join thing_label tl on t.id = tl.thing_id
			where t.id = $1
			group by t.id`

	var t thinger.Thing
	var labels string

	err := s.db.QueryRow(query, id).Scan(&t.ID, &t.Name, &t.ShortDesc, &t.Active, &t.Img, &t.Bought, &labels)
	if err != nil {
		if err == sql.ErrNoRows {
			return t, thinger.ErrNotFound
		}
		return t, err
	}
	if len(labels) > 0 {
		t.Labels = strings.Split(labels, ",")
	}

	return t, nil
}

func (s *postgresStorage) GetThings() ([]thinger.Thing, error) {
	ts := make([]thinger.Thing, 0)

	query := `select t.*, coalesce(string_agg(tl.label_name, ','), '') as labels
					from things t
					left join thing_label tl on t.id = tl.thing_id
					group by t.id`
	rows, err := s.db.Query(query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var t thinger.Thing
		var l string
		err := rows.Scan(&t.ID, &t.Name, &t.ShortDesc, &t.Active, &t.Img, &t.Bought, &l)
		if err != nil {
			return nil, err
		}
		if len(l) > 0 {
			t.Labels = strings.Split(l, ",")
		}
		ts = append(ts, t)
	}
	if err != nil {
		return nil, err
	}
	return ts, nil
}

func (s *postgresStorage) GetLabels() ([]thinger.Label, error) {
	//TODO implements
	return []thinger.Label{}, nil
}

