package storage

import (
	"log"

	"mlf/stuff.control/pkg/thinger"
)

type storedLabel struct {
	ID          string
	Name        string
	Description string
}

type storedThing struct {
	ID        string
	Name      string
	ShortDesc string
	Active    bool
	Img       string
	Bought    string
	Labels    map[string]storedLabel
}

func toLabels(labels []string) map[string]storedLabel {
	stLabels := make(map[string]storedLabel, len(labels))
	for _, label := range labels {
		stLabels[label] = storedLabel{
			Name: label,
		}
	}
	return stLabels
}

func toStrings(stLabels map[string]storedLabel) []string {
	tags := make([]string, 0, len(stLabels))
	for _, l := range stLabels {
		tags = append(tags, l.Name)
	}
	return tags
}

type memStorage struct {
	things map[string]storedThing
	log    *log.Logger
}

// NewMemory creates a new memory storage
// It's a NO-OP implementation. It's just a few maps and it's not thread-safe
func NewMemory(logger *log.Logger) *memStorage {
	return &memStorage{
		things: make(map[string]storedThing),
		log:    logger,
	}
}

func (s *memStorage) AddThing(t thinger.Thing) error {
	labels := toLabels(t.Labels)
	s.things[t.ID] = storedThing{
		ID:        t.ID,
		Name:      t.Name,
		ShortDesc: t.ShortDesc,
		Bought:    t.Bought,
		Active:    t.Active,
		Img:       t.Img,
		Labels:    labels,
	}
	return nil
}

func (s *memStorage) GetThing(id string) (thinger.Thing, error) {
	var t thinger.Thing
	if storedT, ok := s.things[id]; ok {
		t.ID = storedT.ID
		t.Name = storedT.Name
		t.ShortDesc = storedT.ShortDesc
		t.Bought = storedT.Bought
		t.Active = storedT.Active

		return t, nil
	}
	return t, thinger.ErrNotFound
}

func (s *memStorage) GetThings() ([]thinger.Thing, error) {
	things := make([]thinger.Thing, 0, len(s.things))
	for _, v := range s.things {
		labels := toStrings(v.Labels)
		t := thinger.Thing{
			ID:        v.ID,
			Name:      v.Name,
			ShortDesc: v.ShortDesc,
			Bought:    v.Bought,
			Active:    v.Active,
			Labels:    labels,
		}
		things = append(things, t)
	}

	return things, nil
}

func (s *memStorage) GetAll() ([]thinger.Label, error) {
	labels := make([]thinger.Label, 0)
	for _, t := range s.things {
		for _, l := range t.Labels {
			l := thinger.Label{
				Name: l.Name,
			}
			labels = append(labels, l)

		}
	}
	return labels, nil
}
