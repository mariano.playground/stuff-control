package storage

import (
	"context"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"log"
	"mlf/stuff.control/pkg/thinger"
	"time"
)

type mongoStorage struct {
	client *mongo.Client
	things *mongo.Collection
	log    *log.Logger
}

type thing struct {
	ID     string   `bson:"_id,omitempty"`
	Name   string   `bson:"name"`
	Labels []string `bson:"labels"`
}

// NewMongo creates a new database MongoDB
func NewMongo(dbURL string, l *log.Logger) (*mongoStorage, error) {
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)

	client, err := mongo.Connect(ctx, options.Client().ApplyURI(dbURL))
	if err != nil {
		return nil, err
	}

	err = client.Ping(ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}

	things := client.Database("stuff-db").Collection("things")
	return &mongoStorage{client, things, l}, nil
}

func (m *mongoStorage) AddThing(t thinger.Thing) error {
	doc := thing{
		ID:     t.ID,
		Name:   t.Name,
		Labels: t.Labels,
	}
	_, err := m.things.InsertOne(context.TODO(), doc)
	return err
}

func (m *mongoStorage) GetThing(id string) (thinger.Thing, error) {
	var result thing

	err := m.things.FindOne(context.TODO(), bson.D{{"_id", id}}).Decode(&result)
	if err != nil {
		return thinger.Thing{}, err
	}

	return thinger.Thing{
		ID:     result.ID,
		Name:   result.Name,
		Labels: result.Labels,
	}, nil
}

func (m *mongoStorage) GetThings() ([]thinger.Thing, error) {
	results := make([]thinger.Thing, 0)

	cur, err := m.things.Find(context.TODO(), bson.D{{}})
	if err != nil {
		return nil, err
	}
	for cur.Next(context.TODO()) {

		var elem thing
		err := cur.Decode(&elem)
		if err != nil {
			return nil, err
		}

		t := thinger.Thing{
			ID:     elem.ID,
			Name:   elem.Name,
			Labels: elem.Labels,
		}
		results = append(results, t)
	}
	return results, nil
}

func (m *mongoStorage) GetLabels() ([]thinger.Label, error) {
	set := make(map[string]bool, 0)
	filter := bson.D{
		{"labels", bson.D{
			{"$exists", true},
			{"$not", bson.D{
				{"$size", 0},
			}},
		}},
	}
	projection := bson.D{{"labels", 1}}

	cur, err := m.things.Find(context.TODO(), filter, options.Find().SetProjection(projection))
	if err != nil {
		return nil, err
	}

	for cur.Next(context.TODO()) {
		var elem thing
		err := cur.Decode(&elem)
		if err != nil {
			return nil, err
		}
		for _, l := range elem.Labels {
			set[l] = true
		}
	}

	ls := make([]thinger.Label, 0)
	for k := range set {
		ls = append(ls, thinger.Label{
			Name: k,
		})
	}
	return ls, nil
}
