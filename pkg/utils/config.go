package utils

import (
	"github.com/spf13/viper"
	"os"
)

// Config represents the server configuration
type Config struct {
	DBDriver      string `mapstructure:"DB_DRIVER"`
	DBSource      string `mapstructure:"DB_SOURCE"`
	ServerAddress string `mapstructure:"SERVER_ADDRESS"`
	SslCertFile   string `mapstructure:"CERT_FILE"`
	SslKeyFile    string `mapstructure:"KEY_FILE"`
}

var defaultConf = Config{
	DBDriver:      "postgres",
	DBSource:      "postgres://dev:secret@localhost:5432/things_db?sslmode=disable&search_path=public",
	ServerAddress: ":4433",
	SslCertFile:   "./cert/public.crt",
	SslKeyFile:    "./cert/private.key",
}

// LoadConfig loads *.env file located in the received path.
// Missing file will result in a complete default config.
// Missing fields will be set to default  values.
func LoadConfig(path string) (conf Config, err error) {
	viper.SetDefault("DB_DRIVER", defaultConf.DBDriver)
	viper.SetDefault("DB_SOURCE", defaultConf.DBSource)
	viper.SetDefault("SERVER_ADDRESS", defaultConf.ServerAddress)
	viper.SetDefault("CERT_FILE", defaultConf.SslCertFile)
	viper.SetDefault("KEY_FILE", defaultConf.SslKeyFile)

	viper.AddConfigPath(path)
	viper.SetConfigName("app." + getProfile())
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()
	if err != nil {
		if _, ok := err.(viper.ConfigFileNotFoundError); ok {
			return defaultConf, nil
		}
		return
	}
	err = viper.Unmarshal(&conf)
	return
}

func getProfile() string {
	if value, ok := os.LookupEnv("PROFILE"); ok {
		return value
	}
	return "local"
}