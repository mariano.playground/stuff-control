package thinger

// Service is used to manipulate Things
type Service interface {
	AddThing(t Thing) error
	GetThing(id string) (Thing, error)
	GetThings(labels []string) ([]Thing, error)
	GetLabels() ([]Label, error)
}

type things struct {
	repo Storage
}

// NewService returns a fully initialized Service
func NewService(storage Storage) Service {
	return &things{
		repo: storage,
	}
}

func (t *things) AddThing(thing Thing) error {
	return t.repo.AddThing(thing)
}

func (t *things) GetThing(id string) (Thing, error) {
	return t.repo.GetThing(id)
}

func (t *things) GetThings(labels []string) ([]Thing, error) {
	ts := make([]Thing, 0)
	things, err := t.repo.GetThings()
	for _, t := range things {
		if sliceContains(t.Labels, labels) {
			ts = append(ts, t)
		}
	}
	return ts, err
}

func (t *things) GetLabels() ([]Label, error) {
	return t.repo.GetLabels()
}

func sliceContains(s1 []string, s2 []string) bool {
	for _, e := range s2 {
		if !contains(s1, e) {
			return false
		}
	}
	return true
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}
