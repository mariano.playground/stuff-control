package thinger

// Thing is any kind of object.
type Thing struct {
	ID        string   `json:"id"`
	Name      string   `json:"name"`
	ShortDesc string   `json:"description"`
	Active    bool     `json:"active"`
	Img       string   `json:"img"`
	Bought    string   `json:"bought,omitempty"`
	Labels    []string `json:"labels,omitempty"`
}

// Label is used to describe the characteristics or qualities of things.
type Label struct {
	Name      string `json:"name"`
	ShortDesc string `json:"description"`
}
