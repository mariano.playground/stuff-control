package thinger

import "errors"

// ErrNotFound is raised in case a thing is not found
var ErrNotFound = errors.New("thing not found")

// Storage is used to store Things
type Storage interface {
	AddThing(t Thing) error
	GetThing(id string) (Thing, error)
	GetThings() ([]Thing, error)
	GetLabels() ([]Label, error)
}
