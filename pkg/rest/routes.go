package rest

import (
	"fmt"
	"github.com/google/uuid"
	"github.com/gorilla/mux"
	"mlf/stuff.control/pkg/thinger"
	"net/http"
	"strings"
)

type thingHandler struct {
	ts thinger.Service
}

// SetupThingRoutes configures all things routes in the received router
func SetupThingRoutes(r *mux.Router, ts thinger.Service) {
	h := &thingHandler{
		ts: ts,
	}

	// Things
	//TODO add json header middleware to all methods
	r.HandleFunc("/things", h.addThing).Methods(http.MethodPost)
	r.HandleFunc("/things", h.getThings).Methods(http.MethodGet)
	r.HandleFunc("/things/{id}", h.getThing).Methods(http.MethodGet)
	r.HandleFunc("/things/{id}", h.updateThing).Methods(http.MethodPatch)
	// Labels
	r.HandleFunc("/labels", h.getLabels).Methods(http.MethodGet)
}

func (h *thingHandler) addThing(w http.ResponseWriter, r *http.Request) {
	var t thinger.Thing
	if e := fromJSON(r.Body, &t); e != nil {
		jsonError(w, fmt.Sprintf("Unmarshal error: %v", e), http.StatusBadRequest)
		return
	}
	t.ID = uuid.New().String()
	if e := h.ts.AddThing(t); e != nil {
		jsonError(w, fmt.Sprintf("Adding error: %v", e), http.StatusBadRequest)
		return
	}
	w.Header().Add("Location", t.ID)
	w.WriteHeader(http.StatusCreated)
}

func (h *thingHandler) getThings(w http.ResponseWriter, r *http.Request) {
	labels := r.URL.Query().Get("labels")

	split := strings.Split(labels, ",")
	if len(split) == 1 && split[0] == "" {
		split = make([]string, 0)
	}
	things, err := h.ts.GetThings(split)
	if err != nil {
		jsonError(w, fmt.Sprintf("Retrieving error: %v", err), http.StatusBadRequest)
		return
	}
	jsonResponse(w, things)
}

func (h *thingHandler) getThing(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	thing, err := h.ts.GetThing(vars["id"])
	if err != nil {
		status := http.StatusBadRequest
		if err == thinger.ErrNotFound {
			status = http.StatusNotFound
		}
		jsonError(w, fmt.Sprintf("Retrieving error: %v", err), status)
		return
	}
	jsonResponse(w, thing)
}

func (h *thingHandler) updateThing(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	id := vars["id"]

	existingThing, err := h.ts.GetThing(id)
	if err != nil {
		jsonError(w, fmt.Sprintf("Retrieving error: %v", err), http.StatusBadRequest)
		return
	}
	// Update it with the received body
	if err := fromJSON(r.Body, existingThing); err != nil {
		jsonError(w, fmt.Sprintf("Merging error: %v", err), http.StatusBadRequest)
		return
	}
	// Register the updated thing
	if err := h.ts.AddThing(existingThing); err != nil {
		jsonError(w, fmt.Sprintf("Updating error: %v", err), http.StatusBadRequest)
		return
	}
	jsonResponse(w, existingThing)
}

func (h *thingHandler) getLabels(w http.ResponseWriter, r *http.Request) {
	labels, err := h.ts.GetLabels()
	if err != nil {
		jsonError(w, "error while retrieving Labels", http.StatusBadRequest)
		return
	}
	jsonResponse(w, labels)
}
