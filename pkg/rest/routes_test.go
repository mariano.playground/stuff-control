package rest

import (
	"errors"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"mlf/stuff.control/pkg/thinger"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

type mockThingService struct {
	mock.Mock
}

func (m *mockThingService) AddThing(t thinger.Thing) error {
	// I cannot assert the ID because is randomly generated
	t.ID = ""
	args := m.Called(t)
	return args.Error(0)
}

func (m *mockThingService) GetThing(id string) (thinger.Thing, error) {
	args := m.Called(id)
	return args.Get(0).(thinger.Thing), args.Error(1)
}

func (m *mockThingService) GetThings(labels []string) ([]thinger.Thing, error) {
	args := m.Called(labels)
	return args.Get(0).([]thinger.Thing), args.Error(1)
}

func (m *mockThingService) GetLabels() ([]thinger.Label, error) {
	args := m.Called()
	return args.Get(0).([]thinger.Label), args.Error(1)
}

func Test_thingHandler_getThing(t *testing.T) {
	tests := []struct {
		name       string
		thingID    string
		resp       thinger.Thing
		output     string
		statusCode int
		err        error
	}{
		{
			name:       "thing not found",
			thingID:    "mock_id",
			resp:       thinger.Thing{},
			output:     "{\"code\":404, \"message\":\"Retrieving error: thing not found\"}",
			statusCode: http.StatusNotFound,
			err:        thinger.ErrNotFound,
		},
		{
			name:       "get one thing",
			thingID:    "mock_id",
			resp:       thinger.Thing{ID: "mock_id", Name: "mockThing,", Active: true, ShortDesc: "desc"},
			output:     "{\"active\":true, \"description\":\"desc\", \"id\":\"mock_id\", \"img\":\"\", \"name\":\"mockThing,\"}",
			statusCode: http.StatusOK,
		},
		{
			name:       "bad request",
			thingID:    "mock_id",
			resp:       thinger.Thing{},
			output:     "{\"code\":400, \"message\": \"Retrieving error: bad request\"}",
			statusCode: http.StatusBadRequest,
			err:        errors.New("bad request"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assertion := assert.New(t)

			req := httptest.NewRequest(http.MethodGet, "/things/"+tt.thingID, nil)

			mockService := new(mockThingService)
			mockService.On("GetThing", tt.thingID).Return(tt.resp, tt.err)

			r := mux.NewRouter()
			SetupThingRoutes(r, mockService)

			responseRecorder := httptest.NewRecorder()
			r.ServeHTTP(responseRecorder, req)

			assertion.Equal(tt.statusCode, responseRecorder.Code, "httpStatus must be equal")
			assertion.Equal("application/json", responseRecorder.Header().Get("Content-Type"),
				"Content-Type header must be json")
			assertion.JSONEq(tt.output, responseRecorder.Body.String(), "responseBody must be equal")
			mockService.AssertExpectations(t)
		})
	}
}

func Test_thingHandler_getThings(t *testing.T) {
	tests := []struct {
		name       string
		resp       []thinger.Thing
		output     string
		labels     string
		statusCode int
		err        error
	}{
		{
			name:       "get empty things",
			resp:       []thinger.Thing{},
			output:     "[]",
			labels:     "test_label",
			statusCode: http.StatusOK,
		},
		{
			name: "get one thing with multiple labels",
			resp: []thinger.Thing{
				{ID: "mockId", Name: "mockThing,", Active: true, ShortDesc: "desc"},
			},
			output:     "[{\"active\":true, \"description\":\"desc\", \"id\":\"mockId\", \"img\":\"\", \"name\":\"mockThing,\"}]",
			labels:     "label_1,label_2",
			statusCode: http.StatusOK,
		},
		{
			name:       "get bad request",
			resp:       []thinger.Thing{},
			output:     "{\"message\": \"Retrieving error: bad request\", \"code\":400}",
			statusCode: http.StatusBadRequest,
			err:        errors.New("bad request"),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assertion := assert.New(t)

			req := httptest.NewRequest(http.MethodGet, "/things", nil)
			q := req.URL.Query()
			q.Add("labels", tt.labels)
			req.URL.RawQuery = q.Encode()

			mockService := new(mockThingService)
			labels := strings.Split(tt.labels, ",")
			if len(tt.labels) == 0 {
				labels = make([]string, 0)
			}
			mockService.On("GetThings", labels).Return(tt.resp, tt.err)

			r := mux.NewRouter()
			SetupThingRoutes(r, mockService)

			responseRecorder := httptest.NewRecorder()
			r.ServeHTTP(responseRecorder, req)

			assertion.Equal(tt.statusCode, responseRecorder.Code, "httpStatus must be equal")
			assertion.Equal("application/json", responseRecorder.Header().Get("Content-Type"),
				"Content-Type header must be json")
			assertion.JSONEq(tt.output, responseRecorder.Body.String(), "responseBody must be equal")
			mockService.AssertExpectations(t)
		})
	}
}

func Test_thingHandler_addThing(t *testing.T) {
	tests := []struct {
		name       string
		withMock   bool
		jsonInput  string
		input      thinger.Thing
		output     string
		statusCode int
		err        error
	}{
		{
			name:       "add empty thing",
			withMock:   false,
			jsonInput:  "{broken json}}",
			input:      thinger.Thing{},
			output:     "{\"code\":400, \"message\":\"Unmarshal error: invalid character 'b' looking for beginning of object key string\"}",
			statusCode: http.StatusBadRequest,
		},
		{
			name:       "add things",
			withMock:   true,
			jsonInput:  "{\"name\":\"mockThing\",\"description\":\"desc\",\"active\":true}",
			input:      thinger.Thing{ID: "", Name: "mockThing", Active: true, ShortDesc: "desc", Img: "", Bought: "", Labels: nil},
			output:     "{}",
			statusCode: http.StatusCreated,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assertion := assert.New(t)

			req := httptest.NewRequest(http.MethodPost, "/things", strings.NewReader(tt.jsonInput))

			mockService := new(mockThingService)
			if tt.withMock {
				mockService.On("AddThing", tt.input).Return(tt.err)
			}

			r := mux.NewRouter()
			SetupThingRoutes(r, mockService)

			responseRecorder := httptest.NewRecorder()
			r.ServeHTTP(responseRecorder, req)

			assertion.Equal(tt.statusCode, responseRecorder.Code, "httpStatus must be equal")
			//assertion.Equal("application/json", responseRecorder.Header().Get("Content-Type"),
			//	"Content-Type header must be json")
			//assertion.JSONEq(tt.output, responseRecorder.Body.String(), "responseBody must be equal")

			if tt.withMock {
				mockService.AssertExpectations(t)
			}
		})
	}
}

func Test_labelHandler_getLabels(t *testing.T) {
	tests := []struct {
		name       string
		resp       []thinger.Label
		output     string
		statusCode int
		err        error
	}{
		{
			name:       "get empty labels",
			resp:       []thinger.Label{},
			output:     "[]",
			statusCode: http.StatusOK,
			err:        nil,
		},
		{
			name:       "get bad request",
			resp:       nil,
			output:     "{\"message\": \"error while retrieving Labels\", \"code\":400}",
			statusCode: http.StatusBadRequest,
			err:        errors.New("error while retrieving Labels"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assertion := assert.New(t)

			req := httptest.NewRequest(http.MethodGet, "/labels", nil)
			mockService := new(mockThingService)

			mockService.On("GetLabels").Return(tt.resp, tt.err)

			r := mux.NewRouter()
			SetupThingRoutes(r, mockService)

			responseRecorder := httptest.NewRecorder()
			r.ServeHTTP(responseRecorder, req)

			assertion.Equal(tt.statusCode, responseRecorder.Code, "httpStatus must be equal")
			assertion.Equal("application/json", responseRecorder.Header().Get("Content-Type"),
				"Content-Type header must be json")
			assertion.JSONEq(tt.output, responseRecorder.Body.String(), "responseBody must be equal")
			mockService.AssertExpectations(t)
		})
	}
}
