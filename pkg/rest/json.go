package rest

import (
	"encoding/json"
	"io"
	"net/http"
)

func toJSON(w io.Writer, i interface{}) error {
	e := json.NewEncoder(w)
	return e.Encode(i)
}

func fromJSON(r io.Reader, i interface{}) error {
	d := json.NewDecoder(r)
	return d.Decode(i)
}

func jsonResponse(w http.ResponseWriter, i interface{}) {
	w.Header().Set("Content-Type", "application/json")
	if err := toJSON(w, i); err != nil {
		jsonError(w, "Error while marshalling the response", http.StatusInternalServerError)
	}
}

type jsonErr struct {
	Msg  string `json:"message"`
	Code int    `json:"code"`
}

func jsonError(w http.ResponseWriter, msg string, code int) {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.WriteHeader(code)
	toJSON(w, jsonErr{msg, code})
}
