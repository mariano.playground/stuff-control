package rest

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestJsonError(t *testing.T) {
	tests := []struct {
		name string
		msg  string
		code int
	}{
		{
			name: "empty message",
			code: http.StatusBadRequest,
		},
		{
			name: "populated message",
			msg:  "error message",
			code: http.StatusInternalServerError,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assertion := assert.New(t)
			rec := httptest.NewRecorder()
			jsonError(rec, tt.msg, tt.code)

			assertion.Equal(tt.code, rec.Code, "code must be equal")
			assertion.Equal("application/json", rec.Header().Get("Content-Type"),
				"header Content-Type must be json")
			assertion.Equal("nosniff", rec.Header().Get("X-Content-Type-Options"),
				"header X-Content-Type-Options must be nosniff")

			body := fmt.Sprintf("{\"message\":\"%v\",\"code\":%v}", tt.msg, tt.code)
			assertion.JSONEq(body, rec.Body.String(), "body must be equal")
		})
	}
}

func TestJsonResponse(t *testing.T) {
	tests := []struct {
		name string
		resp interface{}
	}{
		{
			name: "empty object",
		},
		{
			name: "map object",
			resp: map[string]string{"key": "value"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assertion := assert.New(t)
			rec := httptest.NewRecorder()
			jsonResponse(rec, tt.resp)

			assertion.Equal("application/json", rec.Header().Get("Content-Type"),
				"header Content-Type must be json")
			exp, _ := json.Marshal(tt.resp)
			assertion.JSONEq(string(exp), rec.Body.String(), "body must be equal")
		})
	}
}
