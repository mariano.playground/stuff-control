BEGIN;

CREATE TABLE IF NOT EXISTS things (
   id UUID PRIMARY KEY,
   name VARCHAR (50) NOT NULL,
   description VARCHAR (300),
   active BOOLEAN,
   img VARCHAR (300),
   bought DATE
);

CREATE TABLE IF NOT EXISTS labels (
   name VARCHAR(25) PRIMARY KEY,
   description VARCHAR (300)
);

CREATE TABLE IF NOT EXISTS thing_label (
    thing_id UUID NOT NULL,
    label_name VARCHAR(25) NOT NULL,
    FOREIGN KEY (thing_id) REFERENCES things(id),
    FOREIGN KEY (label_name) REFERENCES labels(name),
    UNIQUE (thing_id, label_name)
);

COMMIT;