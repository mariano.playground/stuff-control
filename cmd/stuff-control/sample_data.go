package main

import (
	"mlf/stuff.control/pkg/thinger"
)

var sampleThings = []thinger.Thing{
	{
		ID:        "e5fdf088-7fcb-440a-b9f0-bf8a97f3bcc2",
		Name:      "Trousers",
		Bought:    "2006-01-02",
		ShortDesc: "a pair of jeans",
		Active:    true,
		Labels:    []string{"clothes", "traveling"},
	},
	{
		ID:        "da836529-2d56-4210-9710-bbcad5b489fd",
		Name:      "T-Shirt",
		Bought:    "2008-01-02",
		ShortDesc: "a t-shirt",
		Active:    true,
		Labels:    []string{"clothes"},
	},
	{
		ID:        "da836529-2d56-4210-9710-bbcad5b489f2",
		Name:      "T-est",
		Bought:    "2008-01-02",
		ShortDesc: "a t-est",
	},
}
