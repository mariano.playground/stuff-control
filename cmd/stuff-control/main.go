package main

import (
	"context"
	"crypto/tls"
	"log"
	"mlf/stuff.control/pkg/rest"
	"mlf/stuff.control/pkg/storage"
	"mlf/stuff.control/pkg/utils"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"github.com/gorilla/mux"

	"mlf/stuff.control/pkg/thinger"
)

func main() {
	l := log.New(os.Stdout, "stuff-server ", log.LstdFlags)

	conf, err := utils.LoadConfig(".")
	if err != nil {
		log.Fatalf("Error loading service config file. Error %v", err)
	}

	s := newServer(conf, l)

	go func() {
		l.Print("Starting thingsServer")
		if err := s.ListenAndServeTLS("", ""); err != nil {
			l.Printf("%s\n", err)
			os.Exit(1)
		}
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	signal.Notify(c, os.Kill)

	// Block until a signal is received.
	sig := <-c
	l.Println("Got signal:", sig)

	// gracefully shutdown the rest, waiting max 30 seconds for current operations to complete
	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	s.Shutdown(ctx)
}

func newServer(conf utils.Config, log *log.Logger) *http.Server {

	cer, err := tls.LoadX509KeyPair(conf.SslCertFile, conf.SslKeyFile)
	if err != nil {
		log.Printf("%s\n", err)
		os.Exit(1)
	}
	handler, err := handler(conf, log)
	if err != nil {
		log.Fatalf("Error creating the service handler. Error %v", err)
	}

	s := http.Server{
		Addr:         conf.ServerAddress, // configure the bind address
		ErrorLog:     log,                // set the logger for the rest
		ReadTimeout:  5 * time.Second,    // max time to read request from the client
		WriteTimeout: 10 * time.Second,   // max time to write response to the client
		IdleTimeout:  120 * time.Second,  // max time for connections using TCP Keep-Alive
		Handler:      handler,
		TLSConfig: &tls.Config{
			Certificates: []tls.Certificate{cer},
		},
	}

	return &s
}

func handler(conf utils.Config, log *log.Logger) (http.Handler, error) {

	st, err := storage.NewPostgres(conf.DBSource, log)
	if err != nil {
		return nil, err
	}

	thingSrv := thinger.NewService(st)

	for _, t := range sampleThings {
		err := thingSrv.AddThing(t)
		if err != nil {
			log.Printf("Error inserting sample thinger %v. Error %v", t, err)
		}
	}

	r := mux.NewRouter()
	rest.SetupThingRoutes(r, thingSrv)
	return removeTrailingSlash(r), nil
}

func removeTrailingSlash(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			r.URL.Path = strings.TrimSuffix(r.URL.Path, "/")
		}
		next.ServeHTTP(w, r)
	})
}
