PROJECT_NAME := "stuff-control"
PKG := "gitlab.com/mariano.playground/$(PROJECT_NAME)"
#PKG_LIST := $(shell go list ${PKG}/...)
#GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
DB_USER := dev
DB_PASSWORD := secret
DB_NAME := things_db
DOCKER_IMG_NAME := stuff-db
.PHONY: all dep build clean test lint format help postgres createdb initdb initdb teardowndb migrateup migratedown

all: build

# POSTGRES
postgres: ## Create a postgres docker container
	@docker run --name ${DOCKER_IMG_NAME} -p 5432:5432 -e POSTGRES_USER=${DB_USER} -e POSTGRES_PASSWORD=${DB_PASSWORD} -d postgres

postgres-createdb: ## Create a DB in the docker postgres container
	@docker exec -it ${DOCKER_IMG_NAME} createdb --username=${DB_USER} --owner=${DB_USER} ${DB_NAME}

postgres-migrate-install: ## Install migration tool for postgres
	@go install -tags 'postgres' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

postgres-migrateup: ## Apply all UP sql migrations to postgres db
	@migrate -path 'migrations/postgres' -database 'postgres://${DB_USER}:${DB_PASSWORD}@localhost:5432/${DB_NAME}?sslmode=disable&search_path=public' up

postgres-migratedown: ## Apply all DOWN sql migrations to postgres db
	@migrate -path 'migrations/postgres' -database 'postgres://${DB_USER}:${DB_PASSWORD}@localhost:5432/${DB_NAME}?sslmode=disable&search_path=public' down

postgres-start: ## Start postgres db
	@docker start ${DOCKER_IMG_NAME}
	@sleep 1

postgres-init: postgres-start postgres-migrateup ## Start the postgres container and apply all migrations

postgres-teardown: postgres-migratedown ## Apply all Down migrations and stop the postgres container
	@docker stop ${DOCKER_IMG_NAME}

# MONGO
mongo: ## Create a mongo docker container
	@docker run --name ${DOCKER_IMG_NAME}-mongo -p 27017:27017 -e MONGO_INITDB_ROOT_USERNAME=${DB_USER} -e MONGO_INITDB_ROOT_PASSWORD=${DB_PASSWORD} -d mongo

mongo-start: ## Create a db in the mongo container
	@docker start ${DOCKER_IMG_NAME}-mongo

mongo-stop:
	@docker stop ${DOCKER_IMG_NAME}-mongo

mongo-migrate-install: ## Install migration tool for mongodb
	@go install -tags 'mongodb' github.com/golang-migrate/migrate/v4/cmd/migrate@latest

mongo-migrateup:
	@migrate -path 'migrations/mongodb' -database 'mongodb://${DB_USER}:${DB_PASSWORD}@127.0.0.1:27017/${DB_NAME}?authSource=admin&readPreference=primary&gssapiServiceName=mongodb&ssl=false' up

mongo-migratedown:
	@migrate -path 'migrations/mongodb' -database 'mongodb://${DB_USER}:${DB_PASSWORD}@127.0.0.1:27017/${DB_NAME}?authSource=admin&readPreference=primary&gssapiServiceName=mongodb&ssl=false' down

mongo-init: mongo-start mongo-migrateup ## Start the mongo container and apply all migrations

mongo-teardown: mongo-migratedown ## Apply all Down migrations and stop the mongo container
	@docker stop ${DOCKER_IMG_NAME}-mongo

# GO-Stuff
lint: ## Lint the files
	@golint -set_exit_status ./...

# this is NOT-WORKING...YET ;)
format: ## Check code formating with gofmt
	@test -z $(shell gofmt -l .)

test: ## Run unit-tests
	@go test -short -cover ./... -coverprofile .testCoverage.txt

race: dep ## Run data race detector
	@go test -race -short ./...

dep: ## Get the dependencies
	@go mod download
	@go get -u golang.org/x/lint/golint

build: dep ## Build the binary file
	@go build -v cmd/stuff-control/*.go

clean: ## Remove previous build
	@go mod tidy
	@rm -f main
	@rm -f .testCoverage.txt

help: ## Display this help screen
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
