# Stuff Control

## General functionality

 - `things` can have several `labels`
 - `things` can be search by ID and `labels`
 - `thing` can be labeled
   - Via PUT with the thing ID and the `labels`' ids
   - If the one of the `labels` doesn't exist, it can be created
 - `labels` can be listed

 - `Thing` fields:  
   - brand
   - price
   - buying date
   - labels[]
   - owner
   - img
 - Add `label` to `thing`
 - Add/List labels
 - list `thing` with a set of `label`s


## Things API 

 - GET /things -> thing[] - it should have pagination
 - GET /things/{id} -> thing
 - GET /things?labels={label,label} -> thing[]
 - POST /things -> 201 with Location header and body.
 - PUT /things/{id} -> 201 with Location header and body
 - DELTE /things/{id} -> 204
 - GET /labels -> label[] - it should have pagination
