module mlf/stuff.control

go 1.16

require (
	github.com/google/uuid v1.2.0
	github.com/gorilla/mux v1.7.4
	github.com/jackc/pgx/v4 v4.11.0
	github.com/lib/pq v1.10.0 // indirect
	github.com/spf13/viper v1.7.1
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.5.1
	golang.org/x/sync v0.0.0-20201020160332-67f06af15bc9 // indirect
	golang.org/x/sys v0.0.0-20210119212857-b64e53b001e4 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
